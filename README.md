# VRAna - VR Anatomy #

### Team from UT anatomy: ###
* Andres Arend(lead, head of anatomy) [andres.arend@ut.ee]
* Elle Põldoja
* Uku-Laur Tali(anatomy teacher)
* Agu Raudheiding(physicist) [agu.raudheiding@ut.ee]
* Siim Suutre(chair of histology and anatomy, teacher of histology and anatomy)

### Team from czech anatomy: ###
* David Kachlík [david.kachlik@lfmotol.cuni.cz]

### Team from UT iCV: ### 
* Rain Eric Haamer
* Mark [gaudeamusappartments@yahoo.com]
* Mateus Surrage Reis?

### Team from Lodz: ### 
* Tomasz [sapinski.tomasz@gmail.com]
* Grzegorz [grzegorz.zwolinski@p.lodz.pl]
* Kamil

## meeting order every 6 months: ##
* tartu(kickoff) 20-21.1.2020
* ~lodz(technical) 1-15.6.2020~ COVID-19
* ~czech(general) .9.2020~ COVID-19
* lodz(general)
* czech(end)

## Project requirements: ##
* Unity version: 2020.3.24f1
* Very detailed bones
* No histology
* Old bones
* ~~Ragdoll physics and complex rigging alongside~~ static animations
* Minimalistic text boxes with bullet points (text from czech team)
* Undo button
* Laser control preferable but grabbing also
* Standing skeleton but can be rotated on axis
* Zooming
* ~~Normal variance on some bones(multiple copies of bones)~~
* 2 people in VR, identical control
* ~~Student evaluation at the end~~
* ~~Radiological textures~~
* Realistic amorphous joints

## Project: ##
* Models+Animations
* Textures (ET)
* Models (ET)
* Movement (ET)
* Labels (location, text) (ET)
* Environment
* Assets (PL)
* Lighting (PL)
* Post Processing (PL)
* Optimization (PL-Kamil)
* Interaction+UI
* Labels (integration, language) (PL)
* Menus (PL+ET)
* Grab/Point (PL)
* Multiplayer
* 2 players (PL)
* Networking (PL)
* 2 player assets (?)
